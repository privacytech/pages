# PrivacyTech
This is the official git repository of https://privacytech.neocities.org. The source code here is updated weekly based on the changes of the website, so it may not be the latest version. In case the website's server is down, a website fork https://www.privacytech.ml can act as an alternative, which is exactly based on the source code here.
